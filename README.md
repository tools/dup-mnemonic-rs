# DUP Mnemonic

Mnemonic generator

## Install

[Download debian package on releases page](https://git.duniter.org/tools/dup-mnemonic-rs/-/releases)

### Compile from sources

    git clone https://git.duniter.org/tools/dup-mnemonic-rs
    cd dup-mnemonic-rs
    cargo build --release

## Usage

    $ dup-mnemonic [OPTIONS]

Generate a french mnemotic with 12 words:

    $ dup-mnemonic -l fr -w 12
    narquois horde estrade banlieue vigueur poumon subvenir entier chercher cultiver trancher froid

All options :

    -l, --language <language>          Mnemotic language [default: en]  [possible values: en, zh_HANS, zh_HANT, fr, it,
                                       ja, ko, es]
    -o, --output-file <output-file>    Output file, stdout if not present
    -w, --word-count <word-count>      Word count [default: 9]  [possible values: 9, 12, 15, 18, 21, 24]

## Supported languages

| Language | code |
|:-|:-|
| English | en |
| Italian  | fr |
| Spanish  | es |
| Japanese  | ja |
| Korean  | ko |
| ChineseSimplified  | zh_HANS |
| ChineseTraditional  | zh_HANT |
