//  Copyright (C) 2019 Eloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use mnemotic_gen::{Language, Mnemonic, MnemonicType};
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::StructOpt;

static LANGUAGES_CODES: [&str; 8] = ["en", "zh_HANS", "zh_HANT", "fr", "it", "ja", "ko", "es"];
static WORD_COUNT_VALUES: [&str; 6] = ["9", "12", "15", "18", "21", "24"];

#[derive(Debug, StructOpt)]
#[structopt(name = "dup-mnemotic", about = "DUP mnemotic generator")]
struct Opt {
    /// Mnemotic language
    #[structopt(short = "l", long = "language", default_value = "en", possible_values = &LANGUAGES_CODES)]
    language: Language,

    /// Word count
    #[structopt(short = "w", long = "word-count", default_value = "9", possible_values = &WORD_COUNT_VALUES)]
    word_count: usize,

    /// Output file, stdout if not present
    #[structopt(short = "o", long = "output-file", parse(from_os_str))]
    output_file: Option<PathBuf>,
}

fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();

    let mnemotic_type = MnemonicType::for_word_count(opt.word_count).expect("unreachable");

    let mnemonic = Mnemonic::new(mnemotic_type, opt.language);

    if let Some(file_path) = opt.output_file {
        let mut file = File::create(file_path)?;
        file.write_all(mnemonic.phrase().as_bytes())?;
    } else {
        println!("{}", mnemonic.phrase());
    }

    Ok(())
}
