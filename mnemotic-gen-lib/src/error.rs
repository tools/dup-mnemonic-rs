//  Copyright (C) 2019 Eloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::mnemonic_type::MnemonicType;
use failure::Fail;

#[derive(Debug, Fail)]
pub enum ErrorKind {
    #[fail(display = "invalid checksum")]
    InvalidChecksum,
    #[fail(display = "invalid word in phrase")]
    InvalidWord,
    #[fail(display = "invalid keysize: {}", _0)]
    InvalidKeysize(usize),
    #[fail(display = "invalid number of words in phrase: {}", _0)]
    InvalidWordLength(usize),
    #[fail(
        display = "invalid entropy length {}bits for mnemonic type {:?}",
        _0, _1
    )]
    InvalidEntropyLength(usize, MnemonicType),
}
